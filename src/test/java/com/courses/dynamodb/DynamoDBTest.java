package com.courses.dynamodb;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.courses.dynamodb.config.TestConfig;
import com.courses.dynamodb.entity.Order;
import com.courses.dynamodb.service.OrderMapperService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.testcontainers.containers.localstack.LocalStackContainer.Service.DYNAMODB;

@Testcontainers
@SpringBootTest(classes = TestConfig.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
public class DynamoDBTest {

    @Container
    public static LocalStackContainer localStack =
            new LocalStackContainer(DockerImageName.parse("localstack/localstack:0.10.0"))
                    .withServices(DYNAMODB)
                    .withEnv("DEFAULT_REGION", "eu-west-3");

    @Autowired
    private DynamoDBMapper mapper;
    @Autowired
    private OrderMapperService orderMapperService;
    @Autowired
    private AmazonDynamoDB amazonDynamoDB;

    static {
        localStack.start();
    }

    @BeforeAll
    public void init() {
        CreateTableRequest createTableRequest = mapper.generateCreateTableRequest(Order.class);
        ProvisionedThroughput provisionedThroughput = new ProvisionedThroughput(10L, 10L);
        createTableRequest.setProvisionedThroughput(provisionedThroughput);
        createTableRequest.getGlobalSecondaryIndexes().forEach(a -> a.setProvisionedThroughput(provisionedThroughput));

        amazonDynamoDB.createTable(createTableRequest);
    }

    @AfterEach
    public void deleteAll() {
        mapper.scan(Order.class, new DynamoDBScanExpression()).forEach(entity -> mapper.delete(entity));
    }

    @Test
    public void findByNullDescription() {
        mapper.save(new Order().setName("TestName"));
        mapper.save(new Order().setName("TestName").setDescription("description"));

        assertEquals(1, orderMapperService.findByNullDescription().size());
    }

    @Test
    public void findByStatusAndDescription() {
        mapper.save(new Order().setName("TestName").setStatus("In progress").setDescription("Description"));
        mapper.save(new Order().setName("TestName").setStatus("In progress").setDescription("Another description"));

        assertEquals(1, orderMapperService.findByStatusAndDescription("In progress", "Description").size());
    }

    @Test
    public void findByCategoryIndex() {
        mapper.save(new Order()
                .setName("TestName").setStatus("In progress")
                .setDescription("Description").setCategory("SGI"));

        assertEquals(1, orderMapperService.findByCategoryIndex("SGI").size());
    }

}
