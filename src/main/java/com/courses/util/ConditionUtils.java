package com.courses.util;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public class ConditionUtils {

    public static final Condition NULL_CONDITION = new Condition().withComparisonOperator(ComparisonOperator.NULL);

    public @NotNull Condition seqCondition(@NotNull String attributeValue) {
        return new Condition()
                .withComparisonOperator(ComparisonOperator.EQ)
                .withAttributeValueList(new AttributeValue().withS(attributeValue));
    }

}
