package com.courses.dynamodb.controller;

import com.courses.dynamodb.entity.Order;
import com.courses.dynamodb.service.OrderCrudService;
import com.courses.dynamodb.service.OrderMapperService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/orders")
public class OrderController {

    private final OrderCrudService orderService;
    private final OrderMapperService orderMapperService;

    @GetMapping
    public List<Order> orders() {
        return orderService.orders();
    }

    @GetMapping("/id/{id}/name/{name}")
    public Order getOrder(@PathVariable String id, @PathVariable String name) {
        return orderService.order(id, name);
    }

    @PostMapping
    public void save(@RequestBody Order order) {
        orderService.save(order);
    }

    @PutMapping
    public Order update(@RequestBody Order order) {
        return orderService.update(order);
    }

    @DeleteMapping("/id/{id}/name/{name}")
    public void delete(@PathVariable String id, @PathVariable String name) {
        orderService.delete(id, name);
    }

    @GetMapping("/empty-description")
    public List<Order> ordersWithEmptyDescription() {
        return orderMapperService.findByNullDescription();
    }

    @GetMapping("/status-and-description")
    public List<Order> ordersByStatusAndDescription(@RequestParam String status, @RequestParam String description) {
        return orderMapperService.findByStatusAndDescription(status, description);
    }

    @GetMapping("/category/{category}")
    public List<Order> categoryIndex(@PathVariable String category){
        return orderMapperService.findByCategoryIndex(category);
    }

}
