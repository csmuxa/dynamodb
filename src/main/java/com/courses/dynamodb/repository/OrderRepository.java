package com.courses.dynamodb.repository;

import com.courses.dynamodb.entity.Order;
import com.courses.dynamodb.entity.OrderId;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

@EnableScan
public interface OrderRepository extends CrudRepository<Order, OrderId> {

}
