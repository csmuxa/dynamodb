package com.courses.dynamodb.service;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.courses.dynamodb.entity.Order;
import com.courses.util.ConditionUtils;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderMapperService {

    private final DynamoDBMapper mapper;

    public @NotNull List<Order> findByStatusAndDescription(@NotNull String status, @NotNull String description) {
        DynamoDBScanExpression expression = new DynamoDBScanExpression()
                .withFilterConditionEntry("status", ConditionUtils.seqCondition(status))
                .withFilterConditionEntry("description", ConditionUtils.seqCondition(description));

        return mapper.scan(Order.class, expression);
    }

    public @NotNull List<Order> findByNullDescription() {
        DynamoDBScanExpression expression = new DynamoDBScanExpression()
                .withFilterConditionEntry("description", ConditionUtils.NULL_CONDITION);

        return mapper.scan(Order.class, expression);
    }

    public @NotNull List<Order> findByCategoryIndex(@NotNull String category) {
        DynamoDBQueryExpression<Order> expression = new DynamoDBQueryExpression<Order>()
                .withIndexName(Order.CATEGORY_INDEX)
                .withConsistentRead(false)
                .withHashKeyValues(new Order().setCategory(category));

        return mapper.query(Order.class, expression);
    }


}
