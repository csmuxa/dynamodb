package com.courses.dynamodb.service;

import com.courses.dynamodb.entity.Order;
import com.courses.dynamodb.entity.OrderId;
import com.courses.dynamodb.repository.OrderRepository;
import com.courses.util.StreamUtils;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderCrudService {

    private final OrderRepository orderRepository;

    public @NotNull List<Order> orders() {
        return StreamUtils.toStream(orderRepository.findAll())
                .collect(Collectors.toList());
    }

    public @NotNull Order order(@NotNull String id, @NotNull String name) {
        return orderRepository.findById(new OrderId(id, name))
                .orElseThrow(RuntimeException::new);
    }

    public void save(@NotNull Order order) {
        orderRepository.save(order);
    }

    public Order update(@NotNull Order order) {
        Order persistedOrder = order(order.getOrderId().getId(), order.getOrderId().getName());

        persistedOrder.setStatus(order.getStatus()).setDescription(order.getDescription());

        return orderRepository.save(persistedOrder);
    }

    public void delete(@NotNull String id, @NotNull String name) {
        Order persistedOrder = order(id, name);

        orderRepository.delete(persistedOrder);
    }

}
